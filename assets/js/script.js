// 1. Create a registration page
// 2. Registration page should ask the user his:
//   firstname
//   lastname
//   email (@ .)
//   password
//   confirmpassword
//   and checkbox that indicates adherence to the terms and conditions
// 3. password should atleast 8 characters
// 4. the your cannot proceed to the next page if:
//      he has missing details,
//      password is not more than 7 characters or
//      the check box is uncheck
// 5. if the user has complete details he be directed to a thank you page

// create a folder named capstone inside the wd005 folder

const fName = document.getElementById('fName');
const lName = document.getElementById('lName');
const email = document.getElementById('email');
const pass = document.getElementById('pass');
const cPass = document.getElementById('cPass');
const terms = document.getElementById('terms');
const btn = document.querySelector('button');

btn.addEventListener('click', () => {
	if (fName.value.length < 1 || lName.value.length < 1) {
		alert('Please input a name');
	} else if (email.value.length < 5 || !email.value.includes('@') || !email.value.includes('.')) {
		alert('Please input a valid email, email should inclues "@" and "."');
	} else if (pass.value.length < 8) {
		alert('Password should be more than 7 characters');
	} else if (pass.value !== cPass.value || pass.value.length < 1) {
		alert('Password and Confirm Password does not match');
	} else if (!terms.checked) {
		alert('Please agree to terms and condition to continue');
	} else {
		location.href = 'register.html';
	}
});
